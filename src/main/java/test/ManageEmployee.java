package test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class ManageEmployee {

    @Autowired
    private EmployeeRepository employeeRepository;

    public static void main(String[] args) {
        SpringApplication.run(ManageEmployee.class);
    }

    @Bean
    public CommandLineRunner test(EmployeeRepository employeeRepository) {
        return (args) -> {
            employeeRepository.save(new EmployeeEntity("Liam"));
            System.out.println(employeeRepository.findAll());

        };
    }

}